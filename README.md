# Connection between R cran statistical analysis software and Cormas Software

## Basics
* This repository contains:
	* R files 
	* Smalltalk sources

* Works with:
	* Cormas version (see below)
		* 2017.10.10
		* Developer: Pierre Bommel
		* kernel FileName: Kernel-v.2017.10.2
		* VW compatibility: 7.6

	* Example cormas models
		* The SIS model
		* The ECEC model

	* Additionnal parcels that need to be loaded in cormas
		* copy the Add-ons folder in the cormas folder
		* Then load all the parcels below using tools>add-ons>add-on-manager
			* Opentalk-HTTP
			* Opentalk-Tools-Basics
			* Opentalk-Tools-Console
			* Opentalk-Tools-Monitor
			* CormasWS-sayHello.st
			* CormasWS-runSimu.st
			* CormasWS-setAttributeofClassvalue.st
            * DataTransfR.st
            * Root.Smalltalk.CormasNS.Kernel.Entity-rTransfer.st

	* R version 3.4.4 (2018-03-15) with
		* With R packages installed: 
			* httr
			* xml2
			* ggplot2
			* dplyr
			* tidyr
		* NB: For linux you may need to install manualy curl (sudo apt-get install curl) and curlftpfs (sudo apt-get install curlftpfs) package 

## First try

* Open R and set the working directory to the location of this document. The folder must contains:
	* r-cormas-examplr.R file
	* cormas-func.R file

* Make sure that you have all the basics described in above section
(including the additionnal parcels loaded in Cormas and packages installed)

* Follow the tutorial described in the r-cormas-example.R
	* Note that opening of cormas from R does not work at the moment. Cormas needs to be open and you need to open the R cormas connection: Simulation>analysis>R cormas>start webservice.

Then you can use functions of cormas-func.R

## Exple: 
r = openModel("ECEC")
To see the request sent to cormas do: r[[1]]

To see the answer received do : content(r[[2]],"text")
