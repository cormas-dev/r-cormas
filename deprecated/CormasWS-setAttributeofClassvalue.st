<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® NonCommercial, 7.6 of lundi 3 mars 2008 on jeudi 19 octobre 2017 at 3:27:30</time-stamp>


<methods>
<class-id>CormasNS.Kernel.CormasWS</class-id> <category>web-service</category>

<body package="Cormas" selector="setAttribute:ofClass:value:">setAttribute: attName ofClass: aClassName value: aValue
	
	| notThere aCAV aClass |
	"aClassName = symbol du nom de la class"
	"self setAttribute: 'infectiousPeriod' ofClass: Host value: 40"
	&lt;operationName: #SetAttributeOfClassValue &gt;
	&lt;addParameter: #attName type: #String &gt;
	&lt;addParameter: #className type: #ByteString &gt;
	&lt;addParameter: #value type: #Float &gt;
	&lt;result: #String &gt;
	notThere := true.
	aClass:=  myCormas cormasModel classFromSymbol: aClassName .
	(DefaultAttributesSetter attributsFromClass: aClass)
		do: [:n | n = attName ifTrue: [notThere := false]].
	notThere ifTrue: [^attName , 'is not attribute of class ' , aClass name].
	aCAV := ClassAttributeValue newClass: aClass attribute: attName value:
			aValue.
	self myCormas cormasModel defaultAttributesSetter applyNewValue: aCAV.
	myCormas cormasModel recievedNotification.
	^'done'</body>
</methods>

</st-source>
