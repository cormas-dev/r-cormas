<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® NonCommercial, 7.6 of lundi 3 mars 2008 on mercredi 6 novembre 2019 at 1:37:51</time-stamp>


<methods>
<class-id>CormasNS.Kernel.Cormas</class-id> <category>simulation</category>

<body package="Cormas" selector="logMovementOf:from:to:">logMovementOf: anAgentLocation from: srcSpatialEntity to: dstSpatialEntity
	"Log the movement of an agent from a spatial entity to another entity"
	self useExternalVisualization ifTrue: [
		xmlModel sendMoveToExternalVisualizerOf: anAgentLocation from: srcSpatialEntity to: dstSpatialEntity.
	].
	self exportPlaybackLog ifTrue: [
		xmlModel writeMoveToPlaybackLogOf: anAgentLocation from: srcSpatialEntity to: dstSpatialEntity.
	].</body>
</methods>

</st-source>
